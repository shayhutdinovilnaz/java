***Launch: ***
* After building project maven  you can launch project with command -$ mvn spring-boot:run
* Simple client of file service in another repostitory "exb-simple-client". 


1. Objects
	1.1 File contains fields

| Field | Type |Describe |
| ------ | ------ |------ |
| fieldId | String | unique file's id |
| title | String | file's title |
| url | String | link to file for downloading |
| size | Long | file's size |
| ext | String | file's extension |
| date | integer | add date in timestamp |
| type | integer | file's type (1 - txt, 2 - imgage, 3 - gif, 4 - video) |

2. Error codes

		1 - An unknown error has occurred. Please try again later.
		2 - An unknown method was passed.
		3 - User authorization failed.
		4 - Too many requests per second.
		5 - Invalid request.
		6 - Internal server error.

3. API request syntax

	To access the API method, you need to perform a POST or GET or PUT or DELETE request of this type:

	http://localhost:9090/api/v1/method/METHOD_NAME?PARAMETERS


	It consists of several parts:

	METHOD_NAME (required) is the name of the API method you want to access. Note: the method name is case sensitive. For example: "file"
	PARAMETERS (optional) - input parameters of the corresponding API method, a sequence of name = value pairs separated by an ampersand.
	V (required) - used version of the API. Current version is v1

	For example. We want to get file with file id = "file_1213" 

				http://localhost:9090/api/v1/file/get?file_id=file_1213


	In response to such a request, you will receive a response in JSON format:

		[
		    {
		        "fileId": "file_1213",
		        "title": "File",
		        "url": "/Users/ilnaz/Downloads/FILE_SERVICE/files/uploads/Skillsheet-Backend.pdf",
		        "tags": "tag",
		        "size": 642544,
		        "type": 1

		    }
		]

4. File saving process
	1. Transfer the contents of files to the received address in the format multipart / form-data.
	2. Saving information about the downloaded file.

5. Methods

	Each request is needed in autorization. Basic authorization is used in current system. Login = "user", password = "password". 

	5.1 Upload - Transfer the contents of files to the received address in the format multipart / form-data.

		Params:
		file - file in the format multipart / form-data

		Result:
		return file id for in file storage

		Errors:
		 see article - "2. error codes"

		Method: POST

		Exmaple: http://localhost:9090/api/v1/file/upload

	5.2 Save - Saves the file after it has been successfully uploaded to the server.

		Params:
		file_id - parameter returned as a result of uploading the file to the server.
		title  -  title of file
		tags   -  tags are used for search

		Result:
	    Object which describes basic information about file in storage. See. 1.1

		Errors:
		105 - Unable to save file.
		1150 - Invalid document ID.

		And see article - "2. error codes"

		Method: POST

		Exmaple: http://localhost:9090/api/v1/file/save/{file_id}

		Response: In response to such a request, you will receive a response in JSON format:

		[
		    {
		        "fileId": "file_1213",
		        "title": "File",
		        "url": "/Users/ilnaz/Downloads/FILE_SERVICE/files/uploads/Skillsheet-Backend.pdf",
		        "tags": "tag",
		        "size": 642544,
		        "type": 1

		    }
		]

	5.3 Edit - Edit loaded file

		Params:
		file_id - parameter returned as a result of uploading the file to the server.
		title  -  title of file
		tags   -  tags are used for search

		Result:
		After successful execution returns 1.

		Errors:
		1150 - Invalid document ID.
		1152 - Document name not passed.
		1153 - No access to the document.

		And see article - "2. error codes"

		Method: PUT

		Exmaple: http://localhost:9090/api/v1/file/edit/{file_id}

	5.3 Delete - delete file

		Params:
		file_id - parameter returned as a result of uploading the file to the server.

		Result:
		After successful execution returns 1.

		Errors:
		1150 - Invalid document ID.
		1151 - No access to delete documents.

		And see article - "2. error codes"

		Method: DELETE

		Exmaple: http://localhost:9090/api/v1/file/delte/{file_id}

	5.4 Get - Returns the files which have been successfully uploaded to the server by request params.

		Params:
		file_id - Identifiers of documents for which information is to be returned. For example("file_123;file_12")

		Result:
	    Array of objects which describe basic information about file in storage. See. 1.1

		Errors:

		And see article - "2. error codes"

		Method: GET

		Exmaple: http://localhost:9090/api/v1/file/get/{file_id}

		Response: In response to such a request, you will receive a response in JSON format:

		[
		    {
		        "fileId": "file_1213",
		        "title": "File",
		        "url": "/Users/ilnaz/Downloads/FILE_SERVICE/files/uploads/Skillsheet-Backend.pdf",
		        "tags": "tag",
		        "size": 642544,
		        "type": 1

		    },
		    {
		        "fileId": "file_1213",
		        "title": "File",
		        "url": "/Users/ilnaz/Downloads/FILE_SERVICE/files/uploads/Skillsheet-Backend.pdf",
		        "tags": "tag",
		        "size": 642544,
		        "type": 1

		    }
		]


		5.5 Search - Return files which have been successfully uploaded to the server by request params.

		Params:
		q - Query for searching. For example("Kazan")
		count - limit of search result (default value = 1000)
		offset - skip count(default value = 0)

		Result:
	    Array of objects which describe basic information about file in storage. See. 1.1

		Errors:

		And see article - "2. error codes"

		Method: GET

		Exmaple: http://localhost:9090/api/v1/file/search/?q=Kazan&count=10&offset=1

		Response: In response to such a request, you will receive a response in JSON format:

		[
		    {
		        "fileId": "file_1213",
		        "title": "File",
		        "url": "/Users/ilnaz/Downloads/FILE_SERVICE/files/uploads/Skillsheet-Backend.pdf",
		        "tags": "tag",
		        "size": 642544,
		        "type": 1

		    },
		    {
		        "fileId": "file_1213",
		        "title": "File",
		        "url": "/Users/ilnaz/Downloads/FILE_SERVICE/files/uploads/Skillsheet-Backend.pdf",
		        "tags": "tag",
		        "size": 642544,
		        "type": 1

		    }
		]







