package com.exb.main;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.exb.webapp.FileServiceApplication;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FileServiceApplication.class)
public abstract class AbstractTest
{
}
