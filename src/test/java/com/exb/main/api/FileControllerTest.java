package com.exb.main.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.exb.main.AbstractControllerTest;
import com.exb.webapp.clientdto.file.FileClientData;
import com.exb.webapp.model.FileEntity;
import com.exb.webapp.repository.FileRepository;
import com.exb.webapp.service.file.FileService;
import com.exb.webapp.web.v1.FileController;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
public class FileControllerTest extends AbstractControllerTest
{
  @Autowired
  FileRepository fileRepository;

  @Mock
  FileService fileService;

  @InjectMocks
  FileController fileController;

  @Before
  public void init()
  {
    super.setUp();
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void fileSaveTest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/save/{file_id}";
    MvcResult result =
            mvc.perform(MockMvcRequestBuilders.post(uri, fileEntity.getFileId()).param("title", "fileTitle1").param("tags", "tags;tags"))
               .andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 201 HTTP status code", 201, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);

    FileClientData fileClientData = mapFromJson(content, FileClientData.class);
    Assert.assertNotNull(fileClientData);
    Assert.assertEquals(fileClientData.getFileId(), fileEntity.getFileId());
    Assert.assertEquals(fileClientData.getUrl(), fileEntity.getUrl());
  }

  @Test
  public void failFileSaveTest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/save/{file_id}";
    MvcResult result =
            mvc.perform(MockMvcRequestBuilders.post(uri, fileEntity.getId()).param("title", "fileTitle1").param("tags", "tags;tags"))
               .andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 400 HTTP status code", 400, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);
  }

  @Test
  public void fileUpdateTest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/edit/{file_id}";
    MvcResult result =
            mvc.perform(MockMvcRequestBuilders.put(uri, fileEntity.getFileId()).param("title", "editFileTitle").param("tags", "tags;tags"))
               .andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 200 HTTP status code", 200, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);

    Assert.assertEquals("1", content);
  }

  @Test
  public void failFileUpdateTest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/edit/{file_id}";
    MvcResult result =
            mvc.perform(MockMvcRequestBuilders.put(uri, fileEntity.getId()).param("title", "fileTitle1").param("tags", "tags;tags"))
               .andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 400 HTTP status code", 400, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);
  }

  @Test
  public void fileDeleteTest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/delete/{file_id}";
    MvcResult result = mvc.perform(MockMvcRequestBuilders.delete(uri, fileEntity.getFileId())).andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 204 HTTP status code", 204, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);

    Assert.assertEquals("1", content);
  }

  @Test
  public void failFileDeleteTest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/delete/{file_id}";
    MvcResult result = mvc.perform(MockMvcRequestBuilders.delete(uri, fileEntity.getId())).andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 400 HTTP status code", 400, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);
  }

  @Test
  public void wrongRequest() throws Exception
  {
    FileEntity fileEntity = createFileEntity();
    Assert.assertNotNull(fileEntity);
    Assert.assertNotNull(fileEntity.getId());

    String uri = "/api/v1/file/operation/{file_id}";
    MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri, fileEntity.getFileId())).andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 400 HTTP status code", 400, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);
  }

  @Test
  public void getFilesTest() throws Exception
  {
    FileEntity firstFileEntity = createFileEntity();
    FileEntity secondFileEntity = createFileEntity();
    Assert.assertNotNull(firstFileEntity);
    Assert.assertNotNull(secondFileEntity);
    Assert.assertNotNull(firstFileEntity);
    Assert.assertNotNull(secondFileEntity.getId());

    String uri = "/api/v1/file/get";
    MvcResult result =
            mvc.perform(MockMvcRequestBuilders.get(uri).param("file_id", firstFileEntity.getFileId() + ";" + secondFileEntity.getFileId()))
               .andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 200 HTTP status code", 200, statusCode);
    Assert.assertTrue("Failure - expected answer body is not empty", content.trim().length() > 0);
  }

  @Test
  public void notFoundFilesTest() throws Exception
  {

    String uri = "/api/v1/file/get";
    MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).param("file_id", "WRONG_ID")).andReturn();

    String content = result.getResponse().getContentAsString();
    int statusCode = result.getResponse().getStatus();

    Assert.assertEquals("Failure - expected 200 HTTP status code", 200, statusCode);
    Assert.assertEquals("[]", content);
  }


  @Transactional
  FileEntity createFileEntity()
  {
    FileEntity fileEntity = new FileEntity();
    fileEntity.setSize(1);
    fileEntity.setUrl("/temp");
    fileRepository.save(fileEntity);

    String fileId = "file_" + fileEntity.getId();
    fileEntity.setFileId(fileId);
    fileRepository.save(fileEntity);

    return fileEntity;
  }
}
