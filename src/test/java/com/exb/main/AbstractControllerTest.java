package com.exb.main;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.exb.webapp.web.v1.FileController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@WebAppConfiguration
public abstract class AbstractControllerTest extends AbstractTest
{
  protected MockMvc mvc;

  @Autowired
  protected WebApplicationContext webApplicationContext;

  protected void setUp()
  {
    mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  protected void setUp(FileController fileController)
  {
    mvc = MockMvcBuilders.standaloneSetup(fileController).build();
  }

  protected String mapToJson(Object obj) throws JsonProcessingException
  {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(obj);
  }

  protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException
  {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, clazz);
  }
}
