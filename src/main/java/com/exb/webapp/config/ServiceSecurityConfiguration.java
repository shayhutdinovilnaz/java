package com.exb.webapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@Configuration
public class ServiceSecurityConfiguration extends WebSecurityConfigurerAdapter
{
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception
  {
    auth.inMemoryAuthentication().withUser("user").password("{noop}password").roles("USER");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception
  {
    http.httpBasic().and().authorizeRequests().anyRequest().hasRole("USER").and().csrf().disable().formLogin().disable();
  }


}
