package com.exb.webapp.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@Configuration
public class ServiceConfiguration
{
  @Bean
  public CacheManager cacheManager()
  {
    return new EhCacheCacheManager(ehCacheFactory().getObject());
  }

  @Bean
  public EhCacheManagerFactoryBean ehCacheFactory()
  {
    EhCacheManagerFactoryBean bean = new EhCacheManagerFactoryBean();
    bean.setConfigLocation(new ClassPathResource("ehcache.xml"));
    bean.setShared(true);
    return bean;
  }
}
