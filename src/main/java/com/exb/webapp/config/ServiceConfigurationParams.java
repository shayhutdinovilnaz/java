package com.exb.webapp.config;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-10
 */
public interface ServiceConfigurationParams
{
  interface ApiException
  {
    interface Code
    {
      interface Common
      {
        int UNKNOWN_ERROR = 1;
        int UNKNOWN_METHOD_ERROR = 2;
        int AUTHORIZATION_ERROR = 3;
        int TOO_MUCH_REQUEST_ERROR = 4;
        int WRONG_SYNTAX_ERROR = 5;
        int INTERNAL_SERVER_ERROR = 6;
      }

      interface Save
      {
        int UNAVAILABLE_SAVING_ERROR = 105;
      }

      interface Edit
      {
        int WRONG_FILE_ID_ERROR = 1150;
        int EMPTY_FILE_TITLE_ERROR = 1152;
        int NOT_ACCESS = 1153;
      }

      interface Delete
      {
        int DELETE_ACCESS_ERROR = 1151;
      }
    }

    interface Message
    {
      interface Common
      {
        String UNKNOWN_ERROR = "Error. An unknown error has occurred. Please try again later.";
        String UNKNOWN_METHOD_ERROR =
                "Error. An unknown method was passed." + "Check that the name of the method being called is " + "correct.";
        String AUTHORIZATION_ERROR = "Error. User authorization failed.";
        String TOO_MUCH_REQUEST_ERROR = "Error. Too many requests per second. " + "Set a longer interval between calls.";
        String WRONG_SYNTAX_ERROR = "Error. Wrong syntax.";
        String INTERNAL_SERVER_ERROR = "Error. An internal server error occurred. " + "Please try again later.";
      }

      interface Save
      {
        String UNAVAILABLE_SAVING_ERROR = "Error. Unable to save file.";
      }

      interface Edit
      {
        String WRONG_FILE_ID_ERROR = "Invalid file ID";
        String EMPTY_FILE_TITLE_ERROR = "Not given the title of the file";
        String NOT_ACCESS = "The file cannot be accessed.";
      }

      interface Delete
      {
        String DELETE_ACCESS_ERROR = "No access to delete file.";
      }
    }


  }
}
