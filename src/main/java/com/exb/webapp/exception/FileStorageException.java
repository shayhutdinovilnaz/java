package com.exb.webapp.exception;

/**
 * Exception for wrong situation on file storage.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-11
 */
public class FileStorageException extends RuntimeException
{
  public FileStorageException(String message)
  {
    super(message);
  }
}
