package com.exb.webapp.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Exception throws is something wrong happens during work with remote file storage service or can't continue operation.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-10
 */
@Getter
@Setter
public class RestApiException extends Exception
{
  private int code;
  private String message;

  public RestApiException(int code, String message)
  {
    super(message);
    this.code = code;
    this.message = message;
  }

  public RestApiException(String message, Throwable cause, int code)
  {
    super(message, cause);
    this.code = code;
    this.message = message;
  }
}
