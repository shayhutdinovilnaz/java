package com.exb.webapp.exception;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.exb.webapp.config.ServiceConfigurationParams;

/**
 * Build rest api exception according to documentation error code.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-10
 */
public class RestApiExceptionHelper
{
  /**
   * Build api exception with status and message according to documentation.
   *
   * @param exceptionCode - Exception code.
   * @param exception - Instance of exception in system.
   * @return - Object of rest api exception with message and code.
   */
  @Nonnull
  public static RestApiException buildException(int exceptionCode, @Nullable Exception e)
  {
    String message = getExceptionMessage(exceptionCode);
    return new RestApiException(message, e, exceptionCode);
  }

  /**
   * Build api exception with status and message according to documentation.
   *
   * @param exceptionCode - Exception code.
   * @return - Object of rest api exception with message and code.
   */
  @Nonnull
  public static RestApiException buildException(int exceptionCode)
  {
    String message = getExceptionMessage(exceptionCode);
    return new RestApiException(exceptionCode, message);
  }

  private static String getExceptionMessage(int exceptionCode)
  {
    String message = ServiceConfigurationParams.ApiException.Message.Common.UNKNOWN_ERROR;

    switch (exceptionCode)
    {
      case ServiceConfigurationParams.ApiException.Code.Common.UNKNOWN_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Common.UNKNOWN_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Common.UNKNOWN_METHOD_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Common.UNKNOWN_METHOD_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Common.AUTHORIZATION_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Common.AUTHORIZATION_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Common.TOO_MUCH_REQUEST_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Common.TOO_MUCH_REQUEST_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Common.WRONG_SYNTAX_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Common.WRONG_SYNTAX_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Common.INTERNAL_SERVER_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Save.UNAVAILABLE_SAVING_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Save.UNAVAILABLE_SAVING_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Edit.WRONG_FILE_ID_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Edit.WRONG_FILE_ID_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Edit.EMPTY_FILE_TITLE_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Edit.EMPTY_FILE_TITLE_ERROR;
        break;
      case ServiceConfigurationParams.ApiException.Code.Edit.NOT_ACCESS:
        message = ServiceConfigurationParams.ApiException.Message.Edit.NOT_ACCESS;
        break;
      case ServiceConfigurationParams.ApiException.Code.Delete.DELETE_ACCESS_ERROR:
        message = ServiceConfigurationParams.ApiException.Message.Delete.DELETE_ACCESS_ERROR;
        break;
    }
    return message;
  }
}
