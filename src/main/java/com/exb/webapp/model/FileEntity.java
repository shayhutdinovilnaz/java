package com.exb.webapp.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-11
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class FileEntity
{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column(unique = true, nullable = true)
  private String fileId;
  @Column(nullable = false)
  private String url;
  @Column(nullable = true)
  private long size;
  @Column(nullable = true)
  private String tags;
  @Column(nullable = true)
  private String title;
}
