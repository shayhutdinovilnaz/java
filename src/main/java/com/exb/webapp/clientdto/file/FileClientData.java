package com.exb.webapp.clientdto.file;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-09
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FileClientData
{
  private String fileId;
  private String title; // can be nullable
  private String url;
  private String tags; //can be nullable
  private long size;
  private String ext; //can be nullable
  private Date date; //can be nullable
  private int type; //can be nullable

}
