package com.exb.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class FileServiceApplication
{

  public static void main(String[] args)
  {
    SpringApplication.run(FileServiceApplication.class, args);
  }

}
