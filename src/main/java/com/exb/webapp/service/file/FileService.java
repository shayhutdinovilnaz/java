package com.exb.webapp.service.file;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.web.multipart.MultipartFile;

import com.exb.webapp.clientdto.file.FileClientData;
import com.exb.webapp.exception.RestApiException;

/**
 * Service is used for manage files in remote file storage (e.g. upload, update, delete, search). Service calls methods of file
 * integration service for manage files.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
public interface FileService
{
  /**
   * File is uploaded to server storage.
   *
   * @param file - File's for uploading.
   * @return -  File's id in storage service.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  String upload(@Nonnull MultipartFile file) throws RestApiException;

  /**
   * File is saved in remote storage with title and tags for searching.
   *
   * @param id - File's id in storage service.
   * @param title - File's title in storage service.
   * @param tags - File's tags which are use for searching.
   * @return Object which describes basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  FileClientData save(@Nonnull String id, @Nonnull String title, @Nullable String tags) throws RestApiException;

  /**
   * File is updated in remote storage with title and tags for searching without update dates.
   *
   * @param id - File's id in storage service.
   * @param title - File's update title name in storage service.
   * @param tags - File's update tags which are use for searching.
   * @return Object which describes basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  FileClientData update(@Nonnull String id, @Nonnull String title, @Nullable String tags) throws RestApiException;

  /**
   * Files are searched in remote storage by file's ids.
   *
   * @param ids - File's ids in storage service. (example: 10000_a;10001_b;1000)
   * @return Object which describes basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  List<FileClientData> getById(@Nonnull String ids) throws RestApiException;

  /**
   * File is deleted from remote storage by file's id.
   *
   * @param id - File's ids in storage service. (example: 10000_a0)
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  void delete(@Nonnull String id) throws RestApiException;

  /**
   * Files are searched in remote storage by params.
   *
   * @param query - Key word for searching (e.g "yellow hands").
   * @param count - Limit of search result count. Maximum value is 1000.
   * @param offset - Offset in search result.
   * @return Objects which describe basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  List<FileClientData> search(@Nonnull String query, int count, int offset) throws RestApiException;


}
