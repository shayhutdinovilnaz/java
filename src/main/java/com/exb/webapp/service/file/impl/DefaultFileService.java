package com.exb.webapp.service.file.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.exb.webapp.clientdto.file.FileClientData;
import com.exb.webapp.config.ServiceConfigurationParams;
import com.exb.webapp.exception.RestApiException;
import com.exb.webapp.exception.RestApiExceptionHelper;
import com.exb.webapp.integration.service.FileIntegrationService;
import com.exb.webapp.integration.service.RequestOfferFileData;
import com.exb.webapp.integration.service.ResponseOfferFileData;
import com.exb.webapp.service.file.FileService;
import com.exb.webapp.service.file.converter.FileClientConverter;

/**
 * Basic of implementation file service. This service use default file integration service.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
@Service
public class DefaultFileService implements FileService
{
  private final FileIntegrationService fileIntegrationService;
  private final FileClientConverter fileClientConverter;

  public DefaultFileService(FileIntegrationService fileIntegrationService, FileClientConverter fileClientConverter)
  {
    this.fileIntegrationService = fileIntegrationService;
    this.fileClientConverter = fileClientConverter;
  }

  @Nonnull
  @Override
  public String upload(@Nonnull MultipartFile file) throws RestApiException
  {
    RequestOfferFileData requestOfferFileData = new RequestOfferFileData();
    try
    {
      requestOfferFileData.setOriginalName(file.getOriginalFilename());
      requestOfferFileData.setContentType(file.getContentType());
      requestOfferFileData.setSize(file.getSize());
      requestOfferFileData.setBytes(file.getBytes());

    }
    catch (Exception e)
    {
      throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR, e);
    }
    return fileIntegrationService.upload(requestOfferFileData);
  }

  @Nonnull
  @Override
  @CachePut(value = "fileCache", key = "#result.fileId")
  public FileClientData save(@Nonnull String id, @Nonnull String title, @Nullable String tags) throws RestApiException
  {
    List<String> filesTags = null;
    if (Objects.nonNull(tags))
    {
      filesTags = Arrays.asList(tags.split(";"));
    }

    ResponseOfferFileData response = fileIntegrationService.save(id, title, filesTags);
    return fileClientConverter.convert(response);
  }

  @Nonnull
  @Override
  @CachePut(value = "fileCache", key = "#result.fileId")
  public FileClientData update(@Nonnull String id, @Nonnull String title, @Nullable String tags) throws RestApiException
  {
    List<String> filesTags = null;
    if (Objects.nonNull(tags))
    {
      filesTags = Arrays.asList(tags.split(";"));
    }

    ResponseOfferFileData response = fileIntegrationService.update(id, title, filesTags);
    return fileClientConverter.convert(response);
  }

  @Nonnull
  @Override
  public List<FileClientData> getById(@Nonnull String filesId) throws RestApiException
  {
    List<String> filesIds = Arrays.asList(filesId.split(";"));
    return fileIntegrationService.searchById(filesIds).stream().map(fileClientConverter::convert).collect(Collectors.toList());
  }

  @Override
  @CacheEvict(value = "fileCache", key = "#fileId")
  public void delete(@Nonnull String fileId) throws RestApiException
  {
    fileIntegrationService.delete(fileId);
  }

  @Nonnull
  @Override
  public List<FileClientData> search(@Nonnull String query, int count, int offset) throws RestApiException
  {
    return fileIntegrationService.search(query, count, offset).stream().map(fileClientConverter::convert).collect(Collectors.toList());
  }
}
