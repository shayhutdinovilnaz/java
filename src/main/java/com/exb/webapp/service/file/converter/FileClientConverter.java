package com.exb.webapp.service.file.converter;

import org.springframework.stereotype.Service;

import com.exb.webapp.clientdto.file.FileClientData;
import com.exb.webapp.integration.service.ResponseOfferFileData;
import com.exb.webapp.util.Converter;

/**
 * Converter casts to object for sending response to clients.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@Service
public class FileClientConverter implements Converter<ResponseOfferFileData, FileClientData>
{
  @Override
  public FileClientData convert(ResponseOfferFileData source)
  {
    FileClientData fileClientData = new FileClientData();
    fileClientData.setTitle(source.getTitle());
    fileClientData.setSize(source.getSize());
    fileClientData.setTags(source.getTags());
    fileClientData.setFileId(source.getFileId());
    fileClientData.setUrl(source.getUrl());
    return fileClientData;
  }
}
