package com.exb.webapp.repository;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.repository.CrudRepository;

import com.exb.webapp.model.FileEntity;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-11
 */
public interface FileRepository extends CrudRepository<FileEntity, Long>
{
  /**
   * Search file entity by file id.
   *
   * @param fileId - Files id.
   * @return File entity
   */
  @Nullable
  FileEntity findFileEntityByFileId(@Nonnull String fileId);

  /**
   * Search file entities by files ids.
   *
   * @param filesIds - List of files ids.
   * @return List of files entities.
   */
  @Nonnull
  List<FileEntity> findFileEntitiesByFileIdIn(@Nonnull List<String> filesIds);

  /**
   * Delete file entity by id
   *
   * @param fileId - Files id.
   */
  void deleteByFileId(@Nonnull String filesId);
}
