package com.exb.webapp.util;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-09
 */
public interface Converter<SOURCE, TARGET>
{
  TARGET convert(SOURCE source);
}
