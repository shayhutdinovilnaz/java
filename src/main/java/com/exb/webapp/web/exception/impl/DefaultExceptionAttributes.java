package com.exb.webapp.web.exception.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.exb.webapp.exception.RestApiException;
import com.exb.webapp.web.exception.ExceptionAttributes;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
@Service
public class DefaultExceptionAttributes implements ExceptionAttributes
{
  private static final String CODE = "error_code";
  private static final String MESSAGE = "error_msg";
  private static final String REQUEST_PARAMS = "request_params";

  @Nonnull
  @Override
  public Map<String, Object> getExceptionAttributes(@Nonnull RestApiException e, @Nonnull HttpServletRequest servletRequest)
  {
    Map<String, Object> exceptionAttributes = new LinkedHashMap<>();
    exceptionAttributes.put(CODE, e.getCode());
    exceptionAttributes.put(MESSAGE, e.getMessage());

    Map<String, Object> requestParams = new LinkedHashMap<>();
    for (String key : servletRequest.getParameterMap().keySet())
    {
      requestParams.put(key, requestParams.get(key));
    }
    exceptionAttributes.put(REQUEST_PARAMS, requestParams);
    return exceptionAttributes;
  }
}
