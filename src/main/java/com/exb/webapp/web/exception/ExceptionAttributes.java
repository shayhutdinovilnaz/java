package com.exb.webapp.web.exception;

import java.util.Map;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import com.exb.webapp.exception.RestApiException;

/**
 * Populate exception to standard response format of  service API .
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
public interface ExceptionAttributes
{
  /**
   * Convert and populate exception by service standard.
   */
  @Nonnull
  Map<String, Object> getExceptionAttributes(@Nonnull RestApiException e, @Nonnull HttpServletRequest servletRequest);
}
