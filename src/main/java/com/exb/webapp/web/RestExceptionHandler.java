package com.exb.webapp.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.exb.webapp.config.ServiceConfigurationParams;
import com.exb.webapp.exception.RestApiException;
import com.exb.webapp.exception.RestApiExceptionHelper;
import com.exb.webapp.web.exception.ExceptionAttributes;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
@RestControllerAdvice
public class RestExceptionHandler
{
  private final ExceptionAttributes exceptionAttributes;

  public RestExceptionHandler(ExceptionAttributes exceptionAttributes)
  {
    this.exceptionAttributes = exceptionAttributes;
  }

  @ExceptionHandler(AuthenticationException.class)
  public ResponseEntity<Map<String, Object>> handleException(AuthenticationException e, HttpServletRequest request)
  {
    RestApiException restApiException =
            RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR);
    Map<String, Object> responseBody = exceptionAttributes.getExceptionAttributes(restApiException, request);
    return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(RestApiException.class)
  public ResponseEntity<Map<String, Object>> handleException(RestApiException e, HttpServletRequest request)
  {
    Map<String, Object> responseBody = exceptionAttributes.getExceptionAttributes(e, request);
    return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
  }


  @ExceptionHandler(Exception.class)
  public ResponseEntity<Map<String, Object>> handleException(Exception e, HttpServletRequest request)
  {
    RestApiException restApiException =
            RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR);
    Map<String, Object> responseBody = exceptionAttributes.getExceptionAttributes(restApiException, request);
    return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
  }


}
