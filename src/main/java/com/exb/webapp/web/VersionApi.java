package com.exb.webapp.web;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
public interface VersionApi
{
  String VERSION_1 = "/api/v1";
}
