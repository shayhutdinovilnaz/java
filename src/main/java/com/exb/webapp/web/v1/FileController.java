package com.exb.webapp.web.v1;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.exb.webapp.clientdto.file.FileClientData;
import com.exb.webapp.config.ServiceConfigurationParams;
import com.exb.webapp.exception.RestApiException;
import com.exb.webapp.exception.RestApiExceptionHelper;
import com.exb.webapp.service.file.FileService;
import com.exb.webapp.web.VersionApi;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
@RestController
@RequestMapping(VersionApi.VERSION_1 + "/file")
public class FileController
{
  private final FileService fileService;

  public FileController(FileService fileService)
  {
    this.fileService = fileService;
  }

  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) throws RestApiException
  {
    return new ResponseEntity<>(fileService.upload(file), HttpStatus.OK);
  }

  @RequestMapping(value = "/save/{file_id}", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<FileClientData> save(@PathVariable("file_id") String fileId, @RequestParam("title") String title, @RequestParam(value = "tags", required = false, defaultValue = "") String tags)
          throws RestApiException
  {
    return new ResponseEntity<>(fileService.save(fileId, title, tags), HttpStatus.CREATED);
  }

  @RequestMapping(value = "/edit/{file_id}", method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<Integer> edit(@PathVariable("file_id") String fileId, @RequestParam("title") String title, @RequestParam(value = "tags", required = false, defaultValue = "") String tags)
          throws RestApiException
  {
    fileService.update(fileId, title, tags);
    return new ResponseEntity<>(1, HttpStatus.OK);
  }

  @RequestMapping(value = "/delete/{file_id}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<Integer> delete(@PathVariable("file_id") String fileId) throws RestApiException
  {
    fileService.delete(fileId);
    return new ResponseEntity<>(1, HttpStatus.NO_CONTENT);
  }

  @RequestMapping(value = "/get", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<List<FileClientData>> getById(@RequestParam("file_id") String fileId) throws RestApiException
  {
    return new ResponseEntity<>(fileService.getById(fileId), HttpStatus.OK);
  }

  @RequestMapping(value = "/search", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<List<FileClientData>> search(@RequestParam("q") String query, @RequestParam(name = "count", defaultValue = "1000", required = false) int count, @RequestParam(name = "offset", defaultValue = "0", required = false) int offset)
          throws RestApiException
  {
    return new ResponseEntity<>(fileService.search(query, count, offset), HttpStatus.OK);
  }

  @RequestMapping(value = "**", method = {RequestMethod.GET, RequestMethod.POST})
  public void unmapped() throws RestApiException
  {
    throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.UNKNOWN_METHOD_ERROR);
  }

}
