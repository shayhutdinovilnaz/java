package com.exb.webapp.integration.service;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.exb.webapp.exception.RestApiException;

/**
 * Service is uses for manage remote file storage. Service uses API of client remote storage for manage files.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
public interface FileIntegrationService
{
  /**
   * File is uploaded to server storage.
   *
   * @param file - File's for uploading.
   * @return -  File's id in storage service.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  String upload(@Nonnull RequestOfferFileData file) throws RestApiException;

  /**
   * File is saved in remote storage with title and tags for searching.
   *
   * @param filesId - File's id in storage service.
   * @param filesTitle - File's title in storage service.
   * @param filesTags - File's tags which are use for searching.
   * @return Object which describes basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  ResponseOfferFileData save(@Nonnull String filesId, @Nonnull String filesTitle, @Nullable List<String> filesTags) throws RestApiException;

  /**
   * File is updated in remote storage with title and tags for searching without update dates.
   *
   * @param filesId - File's id in storage service.
   * @param filesTitle - File's update title name in storage service.
   * @param filesTags - File's update tags which are use for searching.
   * @return Object which describes basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  ResponseOfferFileData update(@Nonnull String filesId, @Nonnull String filesTitle, @Nullable List<String> filesTags)
          throws RestApiException;

  /**
   * Files are searched in remote storage by file's ids.
   *
   * @param filesIds - List of file's id in storage service.
   * @return Object which describes basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  List<ResponseOfferFileData> searchById(@Nonnull List<String> filesIds) throws RestApiException;

  /**
   * File is deleted from remote storage by file's ids.
   *
   * @param fileId - List of file's id in storage service.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  void delete(@Nonnull String fileId) throws RestApiException;

  /**
   * Files are searched in remote storage by params.
   *
   * @param query - Key word for searching (e.g "yellow hands").
   * @param count - Limit of search result count. Maximum value is 1000.
   * @param offset - Offset in search result.
   * @return Objects which describe basic information about file in storage.
   * @throws RestApiException - throw REST API Exception if happens something wrong during work with remote file storage system or in
   * current system and can't continue operation. More information about exception is enable in message field.
   */
  @Nonnull
  List<ResponseOfferFileData> search(@Nonnull String query, int count, int offset) throws RestApiException;
}
