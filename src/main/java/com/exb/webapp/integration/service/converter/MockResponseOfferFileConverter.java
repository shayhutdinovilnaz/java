package com.exb.webapp.integration.service.converter;

import org.springframework.stereotype.Service;

import com.exb.webapp.integration.service.ResponseOfferFileData;
import com.exb.webapp.model.FileEntity;
import com.exb.webapp.util.Converter;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@Service
public class MockResponseOfferFileConverter implements Converter<FileEntity, ResponseOfferFileData>
{
  @Override
  public ResponseOfferFileData convert(FileEntity source)
  {
    ResponseOfferFileData response = new ResponseOfferFileData();
    response.setFileId(source.getFileId());
    response.setSize(source.getSize());
    response.setTags(source.getTags());
    response.setTitle(source.getTitle());
    response.setUrl(source.getUrl());

    return response;
  }
}
