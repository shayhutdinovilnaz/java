package com.exb.webapp.integration.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Response object in integration process.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ResponseOfferFileData
{
  private String fileId;
  private String title;
  private String url;
  private String tags;
  private long size;
}
