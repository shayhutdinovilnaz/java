package com.exb.webapp.integration.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.exb.webapp.config.ServiceConfigurationParams;
import com.exb.webapp.exception.FileStorageException;
import com.exb.webapp.exception.RestApiException;
import com.exb.webapp.exception.RestApiExceptionHelper;
import com.exb.webapp.integration.service.FileIntegrationService;
import com.exb.webapp.integration.service.RequestOfferFileData;
import com.exb.webapp.integration.service.ResponseOfferFileData;
import com.exb.webapp.integration.service.converter.MockResponseOfferFileConverter;
import com.exb.webapp.model.FileEntity;
import com.exb.webapp.repository.FileRepository;

/**
 * Mock implementation is used for saving file in current system. Mock implementation of service is uses for demonstration.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-08
 */
@Service
public class MockFileIntegrationService implements FileIntegrationService
{
  private final static Logger LOG = LoggerFactory.getLogger(MockFileIntegrationService.class);

  private final Path fileStorageLocation;
  private final FileRepository fileRepository;
  private final MockResponseOfferFileConverter responseConverter;

  public MockFileIntegrationService(FileRepository fileRepository, MockResponseOfferFileConverter responseConverter)
  {
    fileStorageLocation = Paths.get("./files/uploads").toAbsolutePath().normalize();

    try
    {
      Files.createDirectories(fileStorageLocation);
    }
    catch (Exception ex)
    {
      LOG.debug("Could not create the directory where the uploaded files will be stored.");
      throw new FileStorageException("Could not create the directory where the uploaded files will be stored.");
    }
    this.fileRepository = fileRepository;
    this.responseConverter = responseConverter;
  }

  @Transactional(readOnly = false)
  @Nonnull
  @Override
  public ResponseOfferFileData save(@Nonnull String filesId, @Nonnull String filesTitle, @Nullable List<String> filesTags)
          throws RestApiException
  {
    LOG.info("Saving file in storage with file id = " + filesId + ".");
    FileEntity fileEntity = fileRepository.findFileEntityByFileId(filesId);
    if (Objects.nonNull(fileEntity))
    {
      if (Objects.nonNull(filesTags))
      {
        String tags = String.join(";", filesTags);
        fileEntity.setTags(tags);
      }

      fileEntity.setTitle(filesTitle);
      fileRepository.save(fileEntity);
      LOG.info("File was save in storage service. File id = " + filesId + ".");
      return responseConverter.convert(fileEntity);
    }
    LOG.debug("Error during save process. File was not found in storage service. File id = " + filesId + ".");
    throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Save.UNAVAILABLE_SAVING_ERROR);
  }

  @Transactional(readOnly = false)
  @Nonnull
  @Override
  public String upload(@Nonnull RequestOfferFileData file) throws RestApiException
  {
    String fileName = StringUtils.cleanPath(file.getOriginalName());
    LOG.info("Uploading file in storage with file name = " + fileName + ".");

    try
    {
      Path targetLocation = fileStorageLocation.resolve(fileName);
      InputStream is = new ByteArrayInputStream(file.getBytes());
      Files.copy(is, targetLocation, StandardCopyOption.REPLACE_EXISTING);

      FileEntity fileEntity = new FileEntity();
      fileEntity.setSize(file.getSize());
      fileEntity.setUrl(targetLocation.toAbsolutePath().toString());
      fileRepository.save(fileEntity);

      String fileId = "file_" + fileEntity.getId();
      fileEntity.setFileId(fileId);
      fileRepository.save(fileEntity);

      LOG.info("Success. File was uploaded by URL = " + fileEntity.getUrl() + " , file id = " + fileId);
      return fileId;
    }
    catch (IOException ex)
    {
      LOG.error("Error. File is't uploaded. File name =" + fileName, ex);
      throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR);
    }
  }

  @Transactional(readOnly = true)
  @Nonnull
  @Override
  public List<ResponseOfferFileData> searchById(@Nonnull List<String> filesIds) throws RestApiException
  {
    List<FileEntity> fileEntities = fileRepository.findFileEntitiesByFileIdIn(filesIds);
    return fileEntities.stream().map(responseConverter::convert).collect(Collectors.toList());
  }

  @Transactional(readOnly = false)
  @Override
  public void delete(@Nonnull String filesId) throws RestApiException
  {
    FileEntity fileEntity = fileRepository.findFileEntityByFileId(filesId);
    if (Objects.nonNull(fileEntity))
    {
      fileRepository.deleteByFileId(filesId);
    }
    else
    {
      throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Edit.WRONG_FILE_ID_ERROR);
    }
  }

  @Transactional(readOnly = true)
  @Nonnull
  @Override
  public List<ResponseOfferFileData> search(@Nonnull String query, int count, int offset) throws RestApiException
  {
    try
    {
      List<FileEntity> result = new ArrayList<>();
      fileRepository.findAll().forEach(result::add);
      return result.stream().skip(offset).limit(count).map(responseConverter::convert).collect(Collectors.toList());
    }
    catch (Exception e)
    {
      LOG.error(
              "Error.File's is not found. Something a wrong was happened during  search process. Query = " + query + ", limit = " + count +
                      ", offset = " + offset + ".");
      throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR);
    }
  }

  @Transactional(readOnly = false)
  @Override
  public ResponseOfferFileData update(@Nonnull String filesId, @Nonnull String filesTitle, @Nullable List<String> filesTags)
          throws RestApiException
  {
    try
    {
      FileEntity fileEntity = fileRepository.findFileEntityByFileId(filesId);
      if (Objects.nonNull(fileEntity))
      {
        fileEntity.setTitle(filesTitle);
        if (Objects.nonNull(filesTags))
        {
          String tags = String.join(";", filesTags);
          fileEntity.setTags(tags);
        }

        fileRepository.save(fileEntity);
        return responseConverter.convert(fileEntity);
      }

      LOG.debug("Error during update process. File was not found in storage service. File id = " + filesId + ".");
      throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Edit.WRONG_FILE_ID_ERROR);
    }
    catch (Exception e)
    {
      LOG.error("Error during update process. File was not found in storage service. File id = " + filesId + ".");
      throw RestApiExceptionHelper.buildException(ServiceConfigurationParams.ApiException.Code.Common.INTERNAL_SERVER_ERROR);
    }
  }
}
