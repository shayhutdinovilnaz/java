package com.exb.webapp.integration.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Request object in integration process.
 *
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-10
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RequestOfferFileData
{
  private String originalName;
  private byte[] bytes;
  private String contentType;
  private long size;
  private String tags;
}
